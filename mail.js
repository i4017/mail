const express = require('express')
const amqp = require('amqplib/callback_api');
const app = express()

app.use(express.json())

function sendMailToRecipient(recipientEmail, type) {
    var nodemailer = require('nodemailer');

    var transporter = nodemailer.createTransport({
    service: 'Yahoo',
    auth: {
        user: 'refugeehelpers@yahoo.com',
        pass: 'jiqqzhzkepjqfwsr'
    }
    });

    console.log("request type = " + type)

    var emailText = ""
    var emailSubject = ""
    if (type === "request") {
        emailText = "A refugee has requested one of your services!"
        emailSubject = "Service Requested"
    }

    if (type === "accepted") {
        emailText = "A helper has accepted one of your requests!"
        emailSubject = "Request accepted"
    }

    if (type === "declined") {
        emailText = "A helper has declined one of your requests!"
        emailSubject = "Request declined"
    }

    var mailOptions = {
    from: 'refugeehelpers@yahoo.com',
    to: recipientEmail,
    subject: emailSubject,
    text: emailText
    };
    
    transporter.sendMail(mailOptions, function(error, info){
    if (error) {
        console.log(error);
    } else {
        console.log(mailOptions)
        console.log('Email sent: ' + info.response);
    }
    }); 
  }

amqp.connect('amqp://rabbitmq:5672', function(error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function(error1, channel) {
    if (error1) {
      throw error1;
    }
    var queue = 'node_queue';

    channel.assertQueue(queue, {
      durable: true
    });
    channel.prefetch(1);

    console.log("Waiting for messages in %s", queue);
    channel.consume(queue, function(msg) {

      console.log("Received '%s'", msg.content.toString());
      const contactArr = msg.content.toString().split(" ");
      sendMailToRecipient(contactArr[0],contactArr[1])
      setTimeout(function() {
        channel.ack(msg);
      }, 1000);
    });
  });
});


app.listen(8000,() => {console.log("Server started on port 8000")})